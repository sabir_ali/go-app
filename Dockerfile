FROM golang:1.16-alpine

LABEL MAINTAINER=https://github.com/SabirAliyev

WORKDIR /app

COPY go.mod ./
# COPY go.sum ./
RUN go mod download

COPY *.go ./

CMD go run main.go

EXPOSE 8080