package main

import (
	"fmt"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Yay!</h1>")
}

func main() {
	http.HandleFunc("/", handler)

	fmt.Printf("Starting Server...\n")

	log.Fatal(http.ListenAndServe(":8080", nil))
}
